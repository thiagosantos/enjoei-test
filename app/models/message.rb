class Message < ActiveRecord::Base

  default_scope { order(created_at: :desc) }
  scope :archive_all_messages, -> {
    where.not(state: :archived).each { |message| message.archive unless message.archived? }
  }

  validates :title, :content, presence: true

  state_machine :state, initial: :unread do

    event :read do
      transition unread: :read 
    end

    event :archive do
      transition any => :archived
    end

    before_transition to: :read do |message|
      message.read_at = Time.current
    end

    before_transition to: :archived do |message|
      message.archived_at = Time.current
    end
  end
end
